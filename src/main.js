import Vue from "vue";
import VueI18n from "vue-i18n";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import fetch from "node-fetch";
import CountryFlag from 'vue-country-flag'
 
import App from "./App.vue";
import router from "./router";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { languages, defaultLocale } from './i18n/index.js'

Vue.config.productionTip = false;

Vue.prototype.$http = fetch;

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueI18n);
Vue.component('country-flag', CountryFlag)

const i18n = new VueI18n({
  locale: defaultLocale,
  messages: languages,
});

new Vue({
  router,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
