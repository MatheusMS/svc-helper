import en_us from "./en_us.json";
import pt_br from "./pt_br.json";

export const defaultLocale = "en_us";

export const languages = {
  en_us,
  pt_br,
};
