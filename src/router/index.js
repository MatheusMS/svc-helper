import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  // mode: "history", // https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations
  routes: [
    {
      path: "/cache-manager",
      component: require("@/components/CacheManagerPage").default,
      meta: {
        title: "Cache Manager | SVC Helper"
      }
    },
    {
      path: "/pdu-parser",
      component: require("@/components/PduParserPage").default,
      meta: {
        title: "PDU Parser | SVC Helper"
      }
    },
    {
      path: "/google-maps",
      component: require("@/components/GoogleMapsPage").default,
      meta: {
        title: "Google Maps | SVC Helper"
      }
    },
    {
      path: "/settings",
      component: require("@/components/SettingsPage").default,
      meta: {
        title: "Settings | SVC Helper"
      }
    },
    {
      path: "*",
      redirect: "/cache-manager"
    }
  ],
  linkActiveClass: "active",
  linkExactActiveClass: "active"
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || "SVC Helper";
  next();
});

export default router;
