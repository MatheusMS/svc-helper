# svc-helper

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Docker instructions
```
docker build --rm -f "Dockerfile" -t svchelper:latest "."
docker run --rm -d -p 8080:8080/tcp svchelper
```
